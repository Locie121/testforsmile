﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_technical_test.Models
{
        public class CurrencyObject
        {
            public string _base { get; set; }
            public string date { get; set; }
            public int time_last_updated { get; set; }
            public Rates rates { get; set; }
        }
}

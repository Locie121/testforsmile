﻿using dotnet_technical_test.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace dotnet_technical_test.Service
{
    public class CurrencyConvertService : ICurrencyConvertService
    {

        public async Task<decimal> ConvertCurrencyAsync(decimal InputtedAmount, string ConvertFrom, string ConvertTo)
        {

            JObject returnedCurrency = JObject.Parse(await CurrencyEndpoint(ConvertFrom));

            decimal rate = Convert.ToDecimal(returnedCurrency.SelectToken($"rates.{ConvertTo}").ToString());

            decimal floatprice = InputtedAmount * rate;

            decimal finalPrice = Convert.ToDecimal(String.Format("{0:.##}", floatprice));

            return finalPrice;

        }

        // HttpClient is intended to be instantiated once per application, rather than per-use. See Remarks.
        static readonly HttpClient client = new HttpClient();

        public static async Task<string> CurrencyEndpoint(string ConvertFrom)
        {
            // Call asynchronous network methods in a try/catch block to handle exceptions.
            try
            {
                HttpResponseMessage response = await client.GetAsync("https://api.exchangerate-api.com/v4/latest/" +  ConvertFrom);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                // Above three lines can be replaced with new helper method below
                // string responseBody = await client.GetStringAsync(uri);

                return responseBody;
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
            return "";
        }

    }
}

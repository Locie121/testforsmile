﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_technical_test.Service
{
    public interface ICurrencyConvertService
    {

         Task<decimal> ConvertCurrencyAsync(decimal InputtedAmount, string ConvertFrom, string ConvertTo);
            
    }
}

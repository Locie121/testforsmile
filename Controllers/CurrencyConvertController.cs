﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using dotnet_technical_test.Service;

namespace dotnet_technical_test.Controllers
{
    public class CurrencyConvertController : Controller
    {

        private readonly ICurrencyConvertService _currencyConvertService;

        public CurrencyConvertController(ICurrencyConvertService CurrencyConvertService)
        {

            _currencyConvertService = CurrencyConvertService;

        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public Task<decimal> ConvertTotal(decimal InputtedAmount, string ConvertFrom, string ConvertTo)
        {

            Task<decimal> value = _currencyConvertService.ConvertCurrencyAsync(InputtedAmount, ConvertFrom, ConvertTo);

            return value;
        }
    }
}